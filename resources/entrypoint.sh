#!/bin/sh

set -e

SOURCE=""
DESTINATION=""
COUNT=""
TIME=""

if [[ "${DUPLICITY_ACTION}" == "incr" || "${DUPLICITY_ACTION}" == "incremental" || "${DUPLICITY_ACTION}" == "full" ]];
  then
  SOURCE="/data"
fi

if [[ "${DUPLICITY_ACTION}" == "restore" || "${DUPLICITY_ACTION}" == "verify" ]];
  then
  if [[ "${DUPLICITY_ACTION}" == "restore" && ! -z ${DUPLICITY_RESTORED_PATH} ]];
    then
    DESTINATION="/data/${DUPLICITY_RESTORED_PATH}"
  else
    echo -e "\033[0;33mINFO: Datas will be restore on the current data path. You can set “DUPLICITY_RESTORED_PATH” environment variable to restore in a subfolder\033[0m"
    DESTINATION="/data"
  fi
fi

if [[ "${DUPLICITY_ACTION}" == "remove-all-but-n-full" || "${DUPLICITY_ACTION}" == "remove-all-inc-of-but-n-full" ]];
  then
  if [ -z ${DUPLICITY_REMOVE_ALL} ];
    then
    echo -e "\033[0;31m“DUPLICITY_REMOVE_ALL” environment variable must be set with “DUPLICITY_ACTION” “remove-all-but-n-full” or “remove-all-inc-of-but-n-full”\033[0m"
    exit 1
  fi
  COUNT=${DUPLICITY_REMOVE_ALL}
fi

if [[ "${DUPLICITY_ACTION}" == "remove-older-than" ]];
  then
  if [ -z ${DUPLICITY_OLDER_THAN} ];
    then
    echo -e "\033[0;31m“DUPLICITY_OLDER_THAN” environment variable must be set with “DUPLICITY_ACTION” “remove-older-than”\033[0m"
    exit 1
  fi
  TIME=${DUPLICITY_OLDER_THAN}
fi

$(which duplicity) ${DUPLICITY_ACTION} ${SOURCE} ${COUNT} ${TIME} boto3+s3://${S3_BUCKET} ${DESTINATION}\
    --s3-endpoint-url ${S3_ENDPOINT_URL} \
    --s3-european-buckets \
    --s3-use-new-style \
    --encrypt-sign-key ${DUPLICITY_GPG_KEY_ID} \
    --allow-source-mismatch \
    --exclude-device-files \
    --exclude-if-present .nobackup \
    --force \
    --full-if-older-than=${DUPLICITY_FULL_IF_OLDER_THAN} \
    --log-timestamp \
    --archive-dir /tmp/ \
    --progress \
    $@
