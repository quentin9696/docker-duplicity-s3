FROM alpine:3.14

ENV DUPLICITY_VERSION=0.8.19-r0 \
    PYTHON3_VERSION=3.9.5-r1 \
    PYTHON3_BOTO3_VERSION=1.17.93-r0 \
    GNUPGHOME=/.gnupg \
    DUPLICITY_ACTION=incr \
    DUPLICITY_FULL_IF_OLDER_THAN=6M

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

ADD ./resources /resources

RUN /resources/build && rm -rf /resources

VOLUME ["/data"]

WORKDIR /data

ENTRYPOINT ["/usr/local/bin/entrypoint"]

LABEL "maintainer"="quentin9696@vallin.io" \
      "org.label-schema.name"="Duplicity with s3 backend" \
      "org.label-schema.base-image.name"="docker.io/library/alpine" \
      "org.label-schema.base-image.version"="3.14" \
      "org.label-schema.description"="Duplicity with s3 drivers in a container" \
      "org.label-schema.url"="http://duplicity.nongnu.org" \
      "org.label-schema.vcs-url"="http://duplicity.nongnu.org" \
      "org.label-schema.vendor"="quentin9696" \
      "org.label-schema.schema-version"="1.0.0-rc.1" \
      "org.label-schema.applications.duplicity.version"=$DUPLICITY_VERSION \
      "org.label-schema.applications.python3.version"=$PYTHON3_VERSION \
      "org.label-schema.applications.python3-boto3.version"=$PYTHON3_BOTO3_VERSION \
      "org.label-schema.vcs-ref"=$VCS_REF \
      "org.label-schema.version"=$VERSION \
      "org.label-schema.build-date"=$BUILD_DATE \
      "org.label-schema.usage"="docker run --rm $(pwd):/data quentin9696/duplicity-s3 --help"
