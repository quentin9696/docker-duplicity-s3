# Duplicity with S3 drivers

Duplicity with s3 drivers in a container. 

This docker image enable user to use duplicity in a docker image. 

## Usage

In general, you:

* MUST mount what you want to backup or restore in `/data`
* MUST mount you `.gnupg` to be able to encrypt/decrypt your backups
* SHOULD set you docker user id and group using `-u` option
* SHOULD set `AWS_ACCESS_KEY_ID` to let duplicity connect to you S3 bucket
* SHOULD set `AWS_SECRET_ACCESS_KEY` to let duplicity connect to you S3 bucket
* SHOULD set `PASSPHRASE` to let duplicity be able to access to your GPG key

In addition to all [standard duplicity environment variable](http://duplicity.nongnu.org/vers8/duplicity.1.html#sect6), you use:

| environment variable name | description | default value |
|---------------------------|-------------|---------------|
|`DUPLICITY_ACTION` | Action to perform. [More infos](http://duplicity.nongnu.org/vers8/duplicity.1.html#sect4)| incr|
|`S3_BUCKET` | Name of the S3 bucket to interact with| None |
|`S3_ENDPOINT_URL` | URL of the S3 endpoint | None |
|`DUPLICITY_GPG_KEY_ID` | GPG key ID use to encrypt/decrypt/sign backups | None |
|`DUPLICITY_RESTORED_PATH` | Subfolder where duplicity will restore files | None. Restore will be in the current folder |
|`DUPLICITY_REMOVE_ALL`| Number of backup to keep. [More infos](http://duplicity.nongnu.org/vers8/duplicity.1.html#sect4)| None |
|`DUPLICITY_OLDER_THAN` | Number of days to keep backup. [More infos](http://duplicity.nongnu.org/vers8/duplicity.1.html#sect4)| None |
|`DUPLICITY_FULL_IF_OLDER_THAN` | Make a full backup when last full backup is older than | 6M|

## Standard usage

### Backup 

```bash
docker run -it --rm \
  -u $(id -u):$(id -g) \
  -v $(pwd):/data \
  -v ${HOME}/.gnupg:/.gnupg \
  -e AWS_ACCESS_KEY_ID=xxxxxxxxxxxx \
  -e AWS_SECRET_ACCESS_KEY=xxxxxxxxxxxxxxxxxx \
  -e PASSPHRASE=mysecurepassphrase \
  -e S3_BUCKET=mys3bucket/subfolder/subsubfolder \
  -e S3_ENDPOINT_URL='https://s3.wasabisys.com' \
  -e DUPLICITY_GPG_KEY_ID=AAAAAAAAAAAAA \
  -e DUPLICITY_ACTION=incr \
 quentin96/duplicity-s3:latest
```

### Restore

```bash
docker run -it --rm \
  -u $(id -u):$(id -g) \
  -v $(pwd):/data \
  -v ${HOME}/.gnupg:/.gnupg \
  -e AWS_ACCESS_KEY_ID=xxxxxxxxxxxx \
  -e AWS_SECRET_ACCESS_KEY=xxxxxxxxxxxxxxxxxx \
  -e PASSPHRASE=mysecurepassphrase \
  -e S3_BUCKET=mys3bucket/subfolder/subsubfolder \
  -e S3_ENDPOINT_URL='https://s3.wasabisys.com' \
  -e DUPLICITY_GPG_KEY_ID=AAAAAAAAAAAAA \
  -e DUPLICITY_ACTION=restore \
 quentin96/duplicity-s3:latest
```

### Backup collection status

```bash
docker run -it --rm \
  -u $(id -u):$(id -g) \
  -v $(pwd):/data \
  -v ${HOME}/.gnupg:/.gnupg \
  -e AWS_ACCESS_KEY_ID=xxxxxxxxxxxx \
  -e AWS_SECRET_ACCESS_KEY=xxxxxxxxxxxxxxxxxx \
  -e PASSPHRASE=mysecurepassphrase \
  -e S3_BUCKET=mys3bucket/subfolder/subsubfolder \
  -e S3_ENDPOINT_URL='https://s3.wasabisys.com' \
  -e DUPLICITY_GPG_KEY_ID=AAAAAAAAAAAAA \
  -e DUPLICITY_ACTION=collection-status \
 quentin96/duplicity-s3:latest
```

### List files of backup 3 days ago

```bash
docker run -it --rm \
  -u $(id -u):$(id -g) \
  -v $(pwd):/data \
  -v ${HOME}/.gnupg:/.gnupg \
  -e AWS_ACCESS_KEY_ID=xxxxxxxxxxxx \
  -e AWS_SECRET_ACCESS_KEY=xxxxxxxxxxxxxxxxxx \
  -e PASSPHRASE=mysecurepassphrase \
  -e S3_BUCKET=mys3bucket/subfolder/subsubfolder \
  -e S3_ENDPOINT_URL='https://s3.wasabisys.com' \
  -e DUPLICITY_GPG_KEY_ID=AAAAAAAAAAAAA \
  -e DUPLICITY_ACTION=list-current-files \
 quentin96/duplicity-s3:latest \
  --time 3D
```

### Restore in `restored` subfolder

```bash
docker run -it --rm \
  -u $(id -u):$(id -g) \
  -v $(pwd):/data \
  -v ${HOME}/.gnupg:/.gnupg \
  -e AWS_ACCESS_KEY_ID=xxxxxxxxxxxx \
  -e AWS_SECRET_ACCESS_KEY=xxxxxxxxxxxxxxxxxx \
  -e PASSPHRASE=mysecurepassphrase \
  -e S3_BUCKET=mys3bucket/subfolder/subsubfolder \
  -e S3_ENDPOINT_URL='https://s3.wasabisys.com' \
  -e DUPLICITY_GPG_KEY_ID=AAAAAAAAAAAAA \
  -e DUPLICITY_ACTION=collection-status \
  -e DUPLICITY_RESTORED_PATH=/restored \
 quentin96/duplicity-s3:latest \
```

### Keep last 5 backups

```bash
docker run -it --rm \
  -u $(id -u):$(id -g) \
  -v $(pwd):/data \
  -v ${HOME}/.gnupg:/.gnupg \
  -e AWS_ACCESS_KEY_ID=xxxxxxxxxxxx \
  -e AWS_SECRET_ACCESS_KEY=xxxxxxxxxxxxxxxxxx \
  -e PASSPHRASE=mysecurepassphrase \
  -e S3_BUCKET=mys3bucket/subfolder/subsubfolder \
  -e S3_ENDPOINT_URL='https://s3.wasabisys.com' \
  -e DUPLICITY_GPG_KEY_ID=AAAAAAAAAAAAA \
  -e DUPLICITY_ACTION=collection-status \
  -e DUPLICITY_REMOVE_ALL=5 \
 quentin96/duplicity-s3:latest \
```

## Note

All folder with `.nobackup` file will be ignored

## Labels

We set labels on our images with additional information on the image. we follow the guidelines defined at http://label-schema.org/. Visit their website for more information about those labels.
